# Distributions Shiny App

This repo contain the files for the Distributions Shiny app. To preview the app, visit [distributions.inqs.info](http://distributions.inqs.info). 

## Run App on Computer

Download the repository as a zip or tar file and extract it.

Navigate to the extracted folder and select the "App.R" and open it in RStudio.

Click the "Run App" button at the top of the file.

A window should open up with the app. If not, it may be in the Viewer Tab in RStudio.

### Requirements

- Install [R](https://cloud.r-project.org/)

- Install [RStudio](https://rstudio.com/products/rstudio/)

- Install the R packages `shiny`, `ggplot2`, and `gridExtra`

